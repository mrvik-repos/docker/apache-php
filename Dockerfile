FROM php:apache

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev

RUN docker-php-ext-configure gd \
        && docker-php-ext-install -j$(nproc) gd

RUN docker-php-ext-install mysqli

RUN pecl install xdebug

RUN docker-php-ext-enable xdebug \
    && docker-php-ext-enable mysqli \
    && docker-php-ext-enable gd

RUN cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini \
    && echo "xdebug.remote_enable=true" >> /usr/local/etc/php/php.ini \
    && echo "xdebug.remote_host=0.0.0.0" >> /usr/local/etc/php/php.ini

EXPOSE 80
